golang-github-xtaci-smux (1.5.16+ds-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on dh-golang.

  [ Roger Shimizu ]
  * New upstream release 1.5.16
    - Fix shaper prio overflow.
  * debian/control:
    - For golang-github-xtaci-smux-dev add Multi-Arch: foreign

 -- Roger Shimizu <rosh@debian.org>  Thu, 23 Sep 2021 23:24:57 +0900

golang-github-xtaci-smux (1.5.15+ds-1) unstable; urgency=medium

  * New upstream version 1.5.15

 -- Roger Shimizu <rosh@debian.org>  Thu, 03 Dec 2020 03:43:29 +0900

golang-github-xtaci-smux (1.5.14+ds-1) unstable; urgency=medium

  * New upstream version 1.5.14

 -- Roger Shimizu <rosh@debian.org>  Fri, 29 May 2020 22:40:52 +0900

golang-github-xtaci-smux (1.5.12+ds-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Roger Shimizu <rosh@debian.org>  Mon, 04 May 2020 19:01:33 +0900

golang-github-xtaci-smux (1.5.12+ds-1) unstable; urgency=medium

  * New upstream version 1.5.12
  * debian/control and debian/copyright:
    - Update to use my debian email.
  * debian/control:
    - Bump debhelper version to 12.
    - Bump policy version to 4.5.0
  * Add debian/upstream/metadata

 -- Roger Shimizu <rosh@debian.org>  Tue, 21 Apr 2020 01:12:01 +0900

golang-github-xtaci-smux (1.3.5+ds-1) unstable; urgency=medium

  * New upstream version 1.3.5:
    Bug fixes by upstream author @xtaci for intermittent test errors
    detected by Ubuntu autopkgtest:
     - fix incorrect error reporting in tests,
       see https://github.com/xtaci/smux/issues/54
     - adjust test function relates to https://github.com/xtaci/smux/issues/55
  * Update Maintainer email address to team+pkg-go@tracker.debian.org
  * Apply "cme fix dpkg" fixes:
    - Update debhelper dependency to "Build-Depends: debhelper-compat (= 12)"
    - Bump Standards-Version to 4.4.0 (no change)
  * Add myself to the list of Uploaders

 -- Anthony Fok <foka@debian.org>  Wed, 28 Aug 2019 00:22:20 -0600

golang-github-xtaci-smux (1.3.4+ds-1) unstable; urgency=medium

  * New upstream version 1.3.4

 -- Roger Shimizu <rogershimizu@gmail.com>  Fri, 23 Aug 2019 02:10:58 +0900

golang-github-xtaci-smux (1.1.0+ds-2) unstable; urgency=medium

  * debian/watch:
    - Change filenamemangle to match with the filename in archive.

 -- Roger Shimizu <rogershimizu@gmail.com>  Thu, 10 Jan 2019 15:12:04 +0200

golang-github-xtaci-smux (1.1.0+ds-1) unstable; urgency=medium

  * New upstream version 1.1.0

 -- Roger Shimizu <rogershimizu@gmail.com>  Thu, 10 Jan 2019 02:13:31 +0200

golang-github-xtaci-smux (1.0.7+ds-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Michael Stapelberg ]
  * Add debian/gitlab-ci.yml (using salsa.debian.org/go-team/ci/cmd/ci).

  [ Roger Shimizu ]
  * New upstream version 1.0.7
  * debian/control:
    - Bump Priority from extra to optional.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sun, 26 Aug 2018 18:38:56 +0900

golang-github-xtaci-smux (1.0.6+ds-1) unstable; urgency=medium

  * New upstream version 1.0.6
  * debian/watch:
    - Change filenamemangle to match with the filename in archive.
  * debian/control:
    - Set Testsuite to autopkgtest-pkg-go.
    - Bump up standards version to 4.1.1 (with change above).
  * Remove debian/source.lintian-overrides, since it's unused.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sun, 15 Oct 2017 02:10:58 +0900

golang-github-xtaci-smux (1.0.5+ds-1~bpo8+1) jessie-backports-sloppy; urgency=medium

  * Rebuild for jessie-backports-sloppy.
  * debian/control:
    - Use dh-golang in jessie-backports (1.19).

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 15 Jul 2017 20:25:35 +0900

golang-github-xtaci-smux (1.0.5+ds-1~bpo9+1) stretch-backports; urgency=medium

  * Rebuild for stretch-backports.

 -- Roger Shimizu <rogershimizu@gmail.com>  Wed, 05 Jul 2017 23:59:20 +0900

golang-github-xtaci-smux (1.0.5+ds-1) unstable; urgency=medium

  * debian/docs:
    - Add README.md
  * debian/control:
    - Update package description.
  * debian/patches:
    - Add a backport patch from upstream to update tests.
  * Because it doesn't depends on package in experimental, and it's total
    new package, it's safe to upload to unstable now.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sat, 13 May 2017 16:34:35 +0900

golang-github-xtaci-smux (1.0.5+ds-1~exp1) experimental; urgency=medium

  * New upstream 1.0.5
  * debian/patches:
    - Add a backport patch from upstream to update README.md

 -- Roger Shimizu <rogershimizu@gmail.com>  Thu, 30 Mar 2017 23:43:44 +0900

golang-github-xtaci-smux (1.0.4+git20170307+ds-1~exp1) experimental; urgency=medium

  * Initial release (Closes: 852955)

 -- Roger Shimizu <rogershimizu@gmail.com>  Tue, 07 Mar 2017 19:11:04 +0900
